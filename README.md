## Image docker aminelch/PHP

Image qui sert au nouveau site https://hub.docker.com/r/aminelch/php

## tools

- security checker tool
- symfony cli
- wget
- git
- composer
- ffmpeg

## Extensions

- zip
- intl
- redis
- imagick
- xdebug
- pdo
- pdo_pgsql
- pdo_mysql
